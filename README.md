# M3_t1_b1_e1 jpujol




# Index
- ### [L'Exercici 1](#exercici-1)
- ### [L'Exercici 2](#exercici-2)
- ### [L'Exercici 3](#exercici-3)
- ### [L'Exercici 4](#exercici-4)
- ### [L'Exercici 5](#exercici-5)
- ### [L'Exercici 6](#exercici-6)
- ### [L'Exercici 7](#exercici-7)
- ### [L'Exercici 8](#exercici-8)

<br><br>


## **Exercici 1:**
**Cerca al mercat actual un IDE propietari i un de codi lliure on es pugui programar en ANSI C. Comenta, a grans trets, les característiques de cada un d’ells. Creus que el CodeBlocks és un bon IDE per programar en C? Raona la resposta.** 
<br>

* Visual C++:  El que mes cap destacar es el seu editor d'interfície gràfica i l'assistència alhora de programar, fa que sigui fàcil d’utilitzar per a nous programadors

* Dev-C++:  Es un programa de codi lliure, lleuger i eficient, utilitza un compilador MinGW. únicament s’utilitza per a Windows.

* CodeBlock: Es un programa de codi lliure, multiplataforma. Desenvolupada mitjançant el propi llenguatge C++. Es un bon software per a començar ja que el seu sistema te varies funcions per abreviar les instruccions i també ens diu les errades amb un codi d’error i una descripció del mateix

<br><br>

[Tornar a l'index...](#index)

## **Exercici 2:** 
**Mostra els blocs de codi d’un programa en ANSI C. Comenta’ls breument. Quins blocs apareixeran com a mínim en tot programa C?**<br><br>


1. Declaració de llibreries: Son un conjunts de variables en les que el programa reconeix per a poder treballar mes ràpidament, les podem crear  nosaltres o tenim una gama d’elles ja dissenyades.

1. Declaració de constants: Es la posició de la memòria on podem desar un valor d’un tipus determinat.

1. Declaració de tipus: En podem trobar de diferents tipus segons e que volem introduir:

 float, double, int, char, short, int, long, long long

1. Definició d’accions i funcions : Aquí li donarem un valor a les variants anteriors.

1. Declaració de variables: Aquí definirem les línies amb els problemes amb l’ajuda dels tipus abans esmentats.

1. Dentencies executables: Aquí tancarem el escrit amb les corxeres i li ficarem el return 0 per tal que es tanqui el programa quan s’hagi acabat d’executar.

[Tornar a l'index...](#index)
<br><br>

## **Exercici 3:** 
**Explica els tipus de comentaris existents en C i fes un algorisme que exemplifiqui el seu ús.** <br><br>


_Trobem dos tipus de comentaris:_

* // la doble barra ens indica que farem un comentari d’una sola línia.
* /* */ amb aquesta manera definim un comentari de mes d’una línia i si ho tanquem, el programa ens llegira tot el codi com si ho fos per això es important tancar-ho.

//Aquest es un exemple de comentari

[Tornar a l'index...](#index)
<br><br><br>

## **Exercici 4:** 
**Quins són els tipus de dades simples en C? Feu un programa que escrigui a pantalla un valor per cada un dels tipus simples. En els tipus numèrics no heu d’incloure els modificadors short, long, signed i unsigned. Explica i posa un exemple de desbordament d’una variable de tipus enter? Afegiu una captura de la sortida a pantalla.**

2. Numerics: float, double, int
2. Caracters: char
2. Enters: short, int, long, long long
2. Reals: float, double, long double
2. Buit:void (signed, unsigned short, long.

<br><br>


![DADESSIMPLES](/imatges/M3activitat4.png)
<br>
[Tornar a l'index...](#index)
<br><br>


## **Exercici 5:** 
**Feu un programa amb la següent sortida a pantalla:**

El número 77 en octal 

_printf("\nEnter 77 base 8: %o",77);_

El número 65535 en hexadecimal i en majúscules. 

_printf("\nEnter 65535 base 16 lletra majúscula: %X",65535);_

El número 32727 en hexadecimal i en minúscules. 

_printf("\nEnter 32727 base 16: %x",32727);_


El número 7325 amb el signe. 

_printf("\nEnter 7325 amb signe: %+i",7325);_

El número 6754 amb deu dígits i farciment de 0s per l’esquerra. 

_printf("\nEnter 6754  dreta 10 dígits: %.10i",6754);_

El número 456.54378 amb 3 decimals.

_printf("\nReal 456.54378 tres decimals: %.3f",456.54378);_

El teu nom i cognoms. 

_printf("\nCadena (\"Josep Maria Pujol\"): %s","Josep Maria Pujol");_

Els primers 8 caràcters de la sortida del punt anterior. 

_printf("\nCadena (primers 8 caràcters): %.8s","Josep Maria Pujol");_



## Afegiu una captura de la sortida a pantalla. Nota: Per fer aquest exercici us cal llegir el document “2 Tipus Simples en C. El printf” i més concretament l’apartat de formats de sortida.



![printf](/imatges/M3activitat5.png)

<br><br>
[Tornar a l'index...](#index)
<br>

## **Exercici 6:** 
**Explica que són les constants i per a que serveixen. Feu un programa que exemplifiqui l’ús de constants.**

Ens les constants emmagatzem aquella part del programa o memòria la qual no podem modificar en el procés de creació del mateix

La constant es la posició de la memòria a on podem emmagatzemar un valor el qual no podem variar o canviar com les variables, ja que si ho fem ens apareixerà un missatge d’error ja que aquest es el nucli del nostre programa

<br><br>
[Tornar a l'index...](#index)
<br>

## **Exercici 7:** 
**Explica que són les variables i per a que serveixen. Relaciona la memòria de la computadora per explicar-ho. Posa’n exemples. Quan utilitzem les variables? Quina relació hi ha entre una variable i la seva adreça de memòria?**


*Les variables son parts de la memòria en les que podem donar un valor determinat, a diferencia de les constant, aquestes si que poden ser canviades sense que afecti al programa.

*Les variables les utilitzem quan necessitem canviar el valor de les nostres entrades, ja que pot canviar segons si el programa es amb diferents respostes o persones.

*La relació es l’etiqueta que nosaltres fiquem per tal que el processador pugui accedir a aquesta memòria per extreure, reescriure o eliminar part de la mateixa.

<br><br>
[Tornar a l'index...](#index)
<br>

## **Exercici 8:** 
**Cita en forma de taula els operadors relacionals, els aritmètics i els lògics que s’utilitzen en ANSI C. Què és la preferència dels operadors? Posa’n exemples** 




**Relacionals Funció Veritat(V) o Fals(F)**




| Simbol | Definició | exemple | Cert/Fals |
| --- | --- | --- | --- |
| = | Igual a. | 2+2=4  | Fals |
| < | Menor que | 5<10 | Verdader | 
| > | Major que... | 10>11 | Fals | 
| <= | Menor o igual |  15<=22  | Verdader | 
| >= | Major o igual | 10>= 11 | Fals | 
| <> | Diferent o no igual | ‘a’ <> ‘b’ | Verdader | 

<br><br>

**Aritmètics Funció**



| Simbol | Definició | 
| --- | --- |
| + | Suma | 
| - | Resta | 
| _ | Negació | 
| * | Multiplicació | 
| / | Divisió | 
| % | Percentatge | 
| ^ | Exponencial | 

 <br><br>

**Rogics Funció**


| Simbol | Definició | 
| --- | --- |
| == | El mateix |
| != | Diferent |
| && | Afegeixes tots els components |
| || | Una cosa O l’altra |
| !  | Indica negació |

<br><br>


Es diu preferència dels operadors perquè es la manera que té la maquina en d’esquerra a dreta d'avaluar una operació.

<br> <br>

[Tornar a l'index...](#index)







